export class List {
  data: number[] = [];
  // push = (v: number) => this.data.push(v);
  push = (v: number) => this.data.length < 10 ? this.data.push(v) : this.data
  pop = () => this.data.pop()!;
  size = () => this.data.length;
}
