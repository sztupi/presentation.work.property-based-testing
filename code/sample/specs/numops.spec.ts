import * as fc from 'fast-check';

test('+ is commutative and associative', () => {
    const sum = (a:number[]) => a.reduce((p,c) => p+c,0)

    fc.assert(
        fc.property(
            fc.array(fc.double()).chain(a =>
                fc.tuple(fc.constant(a),
                         fc.shuffledSubarray(a,{minLength:a.length}))
                                        )
            , ([array, shuffled]) => {
                expect(sum(array)).toEqual(sum(shuffled))
            })
        // { seed: -1050858935 }
    );
});
