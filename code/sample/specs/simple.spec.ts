import * as fc from 'fast-check';

const sort = a => [...a].sort()

test('for all list : for each (a,b) in pairs(sorted(li)): a <= b', () => {
  fc.assert(
    fc.property(fc.array(fc.integer().map(i => 2*i)), data => {
      const sorted = sort(data)
      for (let i = 0; i < sorted.length - 1; i++) {
        expect(sorted[i]).toBeLessThanOrEqual(sorted[i+1])
      }
    }),
    { verbose: true }
  );
});

test('forall (a,b,c) strings : concat(a,b,c).contains(b)', () => {
  fc.assert(
    fc.property(fc.tuple(fc.string(), fc.string(), fc.string()), ([a,b,c]) => {
      expect(a.concat(b).concat(c).includes(b)).toBeTruthy()
    })
  )
});
